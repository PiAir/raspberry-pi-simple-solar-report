<?php
/**
	Copyright 2014 - Pierre Gorissen - http://ictoblog.nl/
	No rights reserved
	
	Note: make sure you change the $dbpwd variable to reflect the
	password of your own MySQL database.

**/
	function validDate($date, $format = 'Y-m-d') {
			$d = DateTime::createFromFormat($format, $date);
			return $d && $d->format($format) == $date;
	}

    $dbhost="localhost";
    $dblogin="root";
    $dbpwd="YOURPASSWORD";
    $dbname="smatool";
    $dateselect = "";
	$start = 0;
	
    $db =  mysql_connect($dbhost,$dblogin,$dbpwd);
    mysql_select_db($dbname); 
	
	if (isset($_GET['from']) ) {
		$fromdate = $_GET['from'];
		if (validDate($fromdate)) {
			$dateselect = " WHERE DateTime > '".$fromdate."' ";
		}
	}
	if (isset($_GET['to']) ) {
		$todate = $_GET['to'];
		if (validDate($todate)) {
			if ($dateselect == ""){
				$dateselect .= " WHERE ";
			} else {
				$dateselect .= " AND ";
			}		
			$dateselect .= "DateTime < '".$todate."' ";
		}
	}	

	if ($_GET['type'] == "M") {
		$SQLString = "SELECT concat(year(DateTime), '-', month(DateTime)) as data, 
					Sum(CurrentPower) / 10000 as power 
					FROM DayData 
					 ".$dateselect." 
					GROUP BY concat(year(DateTime), '-', month(DateTime)) 
					ORDER BY concat(year(DateTime), '-', LPAD(concat('',month(DateTime)),2,'0')) ASC;";	
	} elseif ($_GET['type'] == "W") {
		$SQLString = "SELECT concat(year(DateTime), '-', weekofyear(DateTime)) as data, 
					Sum(CurrentPower) / 10000 as power 
					FROM DayData 
					 ".$dateselect." 
					GROUP BY concat(year(DateTime), '-', weekofyear(DateTime)) 
					ORDER BY concat(year(DateTime), '-', LPAD(concat('',weekofyear(DateTime)),2,'0')) ASC;";	
	} elseif ($_GET['type'] == "D") {	
		$SQLString = "SELECT 
					date(DateTime) as data,
					Sum(CurrentPower) / 10000 as power
					FROM DayData 
					 ".$dateselect." 
					GROUP BY date(DateTime) 
					ORDER BY date(DateTime) ASC;";		
	} else {
		$SQLString = "SELECT 
					date(DateTime) as day,
					MIN(ETotalToday) as start
					FROM DayData
					GROUP BY date(DateTime)					
					ORDER BY date(DateTime) DESC
					LIMIT 1;";	
		$result = mysql_query($SQLString); 		
		$day = mysql_result($result, 0, "day"); 
		$start = (int) mysql_result($result, 0, "start"); 
			
		$SQLString = "SELECT 
					HOUR(DateTime) as data,
					MAX(ETotalToday) as power
					FROM DayData 
					WHERE date(DateTime) = '".$day."'
					GROUP BY HOUR(DateTime) 
					ORDER BY date(DateTime) ASC;";			
					
	}
	

    $result = mysql_query($SQLString);    
	$num = mysql_num_rows($result);   
	$data[0] = array('data','power');		
    for ($i=1; $i<($num+1); $i++)
    {
		$power = (float) mysql_result($result, $i-1, "power");
		$power =  round($power - $start,3);
		$data[$i] = array(mysql_result($result, $i-1, "data"), 	$power);
    }	
    echo json_encode($data);
    mysql_close($db);
?>