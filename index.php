﻿<?php
/**
	Copyright 2014 - Pierre Gorissen - http://ictoblog.nl/
	No rights reserved

**/

	$url = "data.php";
	$hAxis = "Day";
	$vAxis = "Power (KwH)";
	$curve = "";
	$timefilter = "";
	
	if ($_GET['type'] == "M") {
		$url .= "?type=M";
		$hAxis = "Month";
		$curve = ", curveType: 'function'";
	} elseif ($_GET['type'] == "W") {
		$url .= "?type=W";
		$hAxis = "Week";
		$curve = ", curveType: 'function'";
	} elseif ($_GET['type'] == "D") {
		$url .= "?type=D";
		$hAxis = "Day";
	} else {
		$curve = ", curveType: 'function'";
		$hAxis = "Hour";
		$url .= "?type=L";
		$curve = ", curveType: 'function'";	
	}
	if (isset($_GET['from'])) {
		$timefilter .= "&from=".$_GET['from'];
	}
	if (isset($_GET['to'])) {
		$timefilter .= "&to=".$_GET['to'];
	}	
	$url .= $timefilter;
	
	
?>
<html>
<head>
    <title>Solar data</title>
    <!-- Load jQuery -->
    <script language="javascript" type="text/javascript" 
        src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.0/jquery.min.js">
    </script>
    <!-- Load Google JSAPI -->
    <script type="text/javascript" src="https://www.google.com/jsapi"></script>
    <script type="text/javascript">
        google.load("visualization", "1", { packages: ["corechart"] });
        google.setOnLoadCallback(drawChart);

        function drawChart() {
            var jsonData = $.ajax({
                url: "<?php echo $url; ?>",
                dataType: "json",
                async: false
            }).responseText;

            var obj = jQuery.parseJSON(jsonData);
            var data = google.visualization.arrayToDataTable(obj);

            var options = {
                title: '',
				lineWidth: 4,
				hAxis: {title: '<?php echo $hAxis; ?>'},
				vAxis: {title: '<?php echo $vAxis; ?>', minValue: 0, viewWindow: {min: 0}},
				legend: 'none'
				<?php echo $curve; ?>			
            };

            var chart = new google.visualization.LineChart(
                        document.getElementById('chart_div'));
            chart.draw(data, options);
        }

    </script>
</head>
<body>
    <div style="text-align: center; font-size: 12px; font-family: arial;"><p><a href="?type=L">Live</a> | <a href="?type=D<?php echo $timefilter; ?>">Daily</a> | <a href="?type=W<?php echo $timefilter; ?>">Weekly</a> | <a href="?type=M<?php echo $timefilter; ?>">Monthly</a></p></div>
    <div id="chart_div" style="width: 900px; height: 500px;">
    </div>
</body>
</html>